# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  result = []
  characters = str.chars

  characters.each do |ch|
    if ch == ch.upcase
      result << ch
    end
  end
  result.join
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length % 2 != 0
    middle_index = (str.length / 2)
    return str[middle_index]
  else
    middle_indicies = (str.length/2-1..str.length/2)
    return str[middle_indicies]
  end

end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count("aeiouAEIOU")
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  total = 1
  (1..num).to_a.each do |int|
    total *= int
  end
  total
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  return "" if arr == []

  result = ""
  i = 0
  while i < arr.length - 1
      result << arr[i] + separator
      i += 1
  end
  result << arr[-1]
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = []
  characters = str.chars
  characters.each_with_index do |ch, idx|
    if idx % 2 == 0
      result << ch.downcase
    else
      result << ch.upcase
    end
  end
  result.join
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = []
  array = str.split

  array.each do |word|
    if word.length >= 5
      result << word.reverse!
    else
      result << word
    end
  end
  result.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  array = []
  (1..n).each do |int|
    if int % 3 == 0 && int % 5 == 0
      array << "fizzbuzz"
    elsif int % 3 == 0
      array << "fizz"
    elsif int % 5 == 0
      array << "buzz"
    else
      array << int
    end
  end
  array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  arr.join.reverse!.chars.map! {|el| el.to_i}
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  divisors = []
  (1..num).each do |int|
    if num % int == 0
      divisors << int
    end
  end
    return true if divisors.length == 2
    false
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  factors = []
  (1..num).each do |int|
    if num % int == 0
      factors << int
    end
  end
    factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factor_arr = factors(num)
  prime_factors = []

  factor_arr.each do |number|
    if prime?(number)
      prime_factors << number
    end
  end
  prime_factors

end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = []
  evens = []
  arr.each do |el|
    if el.odd?
      odds << el
    else
      evens << el
    end
  end

    return evens.join.to_i if evens.length == 1
    return odds.join.to_i if odds.length == 1

end
